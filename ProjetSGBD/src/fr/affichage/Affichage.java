package fr.affichage;

import java.io.File;
import java.util.Scanner;

import fr.control.ControlNomFichier;
import fr.metier.GestionTable;
import fr.metier.WriteReadTable;

public class Affichage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Creation du dossier BDD
		 */
		File dir = new File("C:/ENV/BDD");
		dir.mkdir();
		/**
		 * instanciation Gestiontable
		 */
		GestionTable GT = new GestionTable();
		WriteReadTable WR = new WriteReadTable();
		Scanner in = new Scanner(System.in);
		String login="";
		String mdp="";
		
		
		do { 
		login="";
		mdp="";
		System.out.println("Saisissez votre login de sorte � acc�der � la base de donn�es");
		login=in.next();
		System.out.println("Saisissez votre mot-de-passe");
		mdp=in.next();
		} while (!ControlNomFichier.controlLogin(login, mdp));
		
		
		/**
		 * CREATE TABLE
		 */
		in.nextLine();
		String values="";
		String request="";
		String isolateTableName="";
			do {
				isolateTableName="";
				values="";
				request="";
				System.out.println("Saisir une requ�te a");
				request=in.nextLine();
				for (int i=0 ; i<request.length() ; i++) {
					if (request.charAt(i) == '(') {
						break; 
					}
				
					if(i>=20) {
						isolateTableName += request.charAt(i);	
					}
				}
				
				int substringStart=0;
				int substringEnd=0;
				for (int i=0 ; i<request.length() ; i++) {		
					if (request.charAt(i) == '(') {
						substringStart=i+1;
					}
					if(request.charAt(i) == ')') {
						substringEnd=i;
					}
				}
				
				values=request.substring(substringStart, substringEnd);
				values=values.replaceAll(", ",";");
				ControlNomFichier.controlExistenceTable(isolateTableName);
				if(!ControlNomFichier.controlRequest(request)) {
					ControlNomFichier.displayHelp();
				}	
			} while (!ControlNomFichier.controlRequest(request) && ControlNomFichier.controlExistenceTable(isolateTableName));
			if(ControlNomFichier.controlRequest(request) && !ControlNomFichier.controlExistenceTable(isolateTableName)) {
				GT.create(isolateTableName, request, values);
				WR.writeFile(isolateTableName, values);
				System.out.println("Fichier cr��");
			}
			
			/**
			 * INSERT INTO
			 */
			in.nextLine();
			String searchTableName="";
			request="";
			String insertValues="";
			do {
				searchTableName="";
				insertValues="";
				request="";
				System.out.println("Saisir une requ�te");
				request=in.nextLine();
				for (int i=0 ; i<request.length() ; i++) {
					if (i>19 && request.charAt(i) == ' ') {
						break; 
					}
				
					if(i>=19) {
						searchTableName += request.charAt(i);	
					}
				}
				
				int substringStart=0;
				int substringEnd=0;
				for (int i=0 ; i<request.length() ; i++) {		
					if (request.charAt(i) == '(') {
						substringStart=i+1;
					}
					if(request.charAt(i) == ')') {
						substringEnd=i;
					}
				}
				
				insertValues=request.substring(substringStart, substringEnd);
				insertValues=insertValues.replaceAll(", ",",");
				ControlNomFichier.controlExistenceInsertDisplay(searchTableName);
				if(!ControlNomFichier.controlRequest(request)) {
					ControlNomFichier.displayHelp();
				}	
			} while (!ControlNomFichier.controlRequest(request) && ControlNomFichier.controlExistenceInsertDisplay(searchTableName));
			if(ControlNomFichier.controlRequest(request) && !ControlNomFichier.controlExistenceInsertDisplay(searchTableName)) {
				WR.writeExistingFile(searchTableName, insertValues);
				System.out.println("Donn�es ins�r�es dans le tableau");
			}
		
		/**
		 * Affichage
		 */
		in.nextLine();
		String requestDisplay="";
		String searchTableDisplay="";
		do {
			searchTableDisplay="";
			requestDisplay="";
			System.out.println("Saisissez une requete!");
			 requestDisplay=in.nextLine();
			 for (int i=0 ; i<requestDisplay.length() ; i++) {
				if(i>=21) {
					searchTableDisplay += requestDisplay.charAt(i);	
				}
			}
			
		} while (!ControlNomFichier.controlRequest(requestDisplay) && ControlNomFichier.controlExistenceInsertDisplay(searchTableName));
		if(ControlNomFichier.controlRequest(requestDisplay) && !ControlNomFichier.controlExistenceInsertDisplay(searchTableName)) {
			GT.read(searchTableDisplay);
			
			
		}
	}

}
