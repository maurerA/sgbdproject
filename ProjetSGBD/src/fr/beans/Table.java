package fr.beans;

public class Table {
private String ligne;
private int nbreColonne;
private String nomTable;

public Table() {
	super();
}

public Table(String nomTable, int nbreColonne, String ligne) {
	super();
	this.ligne= ligne;
	this.nbreColonne = nbreColonne;
	this.nomTable = nomTable;
}



public String getLigne() {
	return ligne;
}

public void setLigne(String ligne) {
	this.ligne = ligne;
}

public int getNbreColonne() {
	return nbreColonne;
}

public void setNbreColonne(int nbreColonne) {
	this.nbreColonne = nbreColonne;
}

public String getNomTable() {
	return nomTable;
}

public void setNomTable(String nomTable) {
	this.nomTable = nomTable;
}

@Override
public String toString() {
	return "Table [ligne=" + this.ligne + ", nbreColonne=" + this.nbreColonne + ", nomTable=" + this.nomTable + "]";
}


public boolean equals(Object o) {
	return this.nomTable.equals(((Table)o).getNomTable());
}


}
