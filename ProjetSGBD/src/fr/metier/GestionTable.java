package fr.metier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import fr.beans.Table;
import fr.control.ControlNomFichier;

public class GestionTable implements ControlNomFichier  {
private Table table;

public GestionTable() {
	super();
}



	public void create(String isolateTableName, String request, String values) {

		String [] isolateComma=request.split(",");
		int columnNumbers=isolateComma.length;
	
		new Table(isolateTableName, columnNumbers, values);
		
		}
/**
 * methode permettant de lire dans un tableau physique les donn�es des tables de donn�es
 * @param tab
 */
	public void read(String searchTableDisplay) {
		FileReader fr = null;
		BufferedReader br = null;
		String val ="";
		String [] tab = null;
		int nbLigne=0;
		
		try {
			fr=new FileReader("C://ENV//BDD//" + searchTableDisplay + ".cda");
			br = new BufferedReader(fr);
				
				val = br.readLine();
				nbLigne = Integer.valueOf(val);
				tab=new String [nbLigne];
				for (int i = 0; i <= nbLigne; i++) {
					
					tab[i]= br.readLine();
										
					}
			
		} catch (Exception e) {
			System.out.println("Erreur"+e);
		}finally {if(br!=null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

		String pointille = "+-------------------------+";
		int total=0;
		int nombre =0;
		
			String []t = tab[0].split(";");
		if (tab.length==0) {
			System.out.println("La table de donn�e est vide.");
		}else {
			for (int i = 0; i < t.length; i++) {
			System.out.print(pointille);}
			System.out.println();
				for (int j = 0; j < t.length; j++) {
					t[j]="|"+t[j];
					nombre = t[j].length();
					total = 25-nombre;	
				 		for (int i = 0; i <= total; i++) {
				 			t[j]=t[j]+" ";
				 		}
				 t[j]=t[j]+"|";
				 System.out.print(t[j]);
			}
				System.out.println();
				for (int i = 0; i < t.length; i++) {
					System.out.print(pointille);
				}
				System.out.println();
			for (int i = 1; i < tab.length; i++) {
				t=tab[i].split(";");
				for (int j = 0; j < t.length; j++) {
					t[j]="|"+t[j];
					nombre = t[j].length();
					total=25-nombre;
				
				for (int k = 0; k <= total; k++) {
					t[j]=t[j]+" ";
				}
				t[j]=t[j]+"|";
				System.out.print(t[j]);
				
				}
				System.out.println();
			}
			for (int i = 0; i < t.length; i++) {
				System.out.print(pointille);}
			}
		}
	
	
	
/**
 * Methode permettant d afficher les donn�es d un parametre de la table de donn�es
 * @param tab
 * @param wantedValue
 */
	public void displayWantedValue(String tab[],String wantedValue) {
		String pointille = "+-------------------------+";
		int total=0;
		int nombre =0;
		int compteur=0;
		if (tab.length==0) {
			System.out.println("La table de donn�e est vide.");}
		else {
			String []t = tab[0].split(";");
			for (int i = 0; i < t.length; i++) {
				if (t[i].equalsIgnoreCase(wantedValue)) {
					compteur=i;
					System.out.println(pointille);
					 t[i]="|"+t[i];
					 nombre = t[i].length();
					 total = 25-nombre;
					for (int j = 0; j <= total; j++) {
						t[i]=t[i]+" ";
					}
					t[i]=t[i]+"|";
					System.out.println(t[i]);
				}
				
			}
			System.out.println(pointille);
			for (int i = 1; i < tab.length; i++) {
				t=tab[i].split(";");
				t[compteur]="|"+t[compteur];
				 nombre = t[compteur].length();
				 total = 25-nombre;
				for (int j = 0; j <= total; j++) {
					t[compteur]=t[compteur]+" ";
				}
				t[compteur]=t[compteur]+"|";
				System.out.println(t[compteur]);
			}
				System.out.println(pointille);
	}
	}
	public void displayWantedValue(String tab[],String wantedValue,String wantedValue1) {
		String pointille ="+-------------------------+";
		int nombre = 0;
		int nombre1=0;
		int total = 0;
		int total1 = 0;
		int compteur=0;
		int compteur1=0;
		
		if (tab.length==0) {
			System.out.println("Le tableau est soit vide ou la donn�e saisie en param�tre n'est pas reconnue.");
		}else {
			String []t = tab[0].split(";");
			for (int i = 0; i < t.length; i++) {
				if (t[i].equalsIgnoreCase(wantedValue)) {
					compteur=i;
				}else if(t[i].equalsIgnoreCase(wantedValue1)) {
					compteur1=i;
				}
							}
				
				nombre=t[compteur].length();
				nombre1=t[compteur1].length();
				total=25-nombre;
				total1=25-nombre1;
				for (int j = 0; j < total; j++) {
					t[compteur]=t[compteur]+" ";
				}
				for (int j = 0; j < total1; j++) {
					t[compteur1]=t[compteur1]+" ";
				}
				
			
			System.out.println(pointille+pointille);
			t[compteur]="|"+t[compteur];
			t[compteur1]="|"+t[compteur1];
			t[compteur]=t[compteur]+"|";
			t[compteur1]=t[compteur1]+"|";
			System.out.println(t[compteur]+t[compteur1]);
			System.out.println(pointille+pointille);	
			for (int i = 1; i < tab.length; i++) {
				t=tab[i].split(";");
				t[compteur]="|"+t[compteur];
				t[compteur1]="|"+t[compteur1];
				
			nombre=t[compteur].length();
			nombre1=t[compteur1].length();
			total=25-nombre;
			total1=25-nombre1;
			for (int k = 0; k <= total; k++) {
				t[compteur]=t[compteur]+" ";
							}
			for (int j = 0; j <= total1; j++) {
				t[compteur1]=t[compteur1]+" ";
				
			}
			t[compteur]=t[compteur]+"|";
			t[compteur1]=t[compteur1]+"|";
			System.out.println(t[compteur]+t[compteur1]);
			System.out.println(pointille+pointille);
		}}
	}
	

	public void insertValue(Table insert) {
	
	}

	public void displayASC(Table displayASC) {
		
	}

	public void displayDESC(Table displayDESC) {
	
	}

	public void setWhere(Table setWhere) {
	
	}

	public void setNValues(Table setNValues) {
	
	}

	public void setOneValue(Table oneValue) {
	
	}

	public Table getTable() {
	return table;
	}

	public void setTable(Table table) {
	this.table = table;
	}

	@Override
	public String toString() {
		return "GestionTable [table=" + table + "]";
	}

}
