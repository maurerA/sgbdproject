package fr.metier;

import java.util.ResourceBundle;

public interface Proprietes {
	
	public static boolean testConfig(String mdp) {
		boolean verif=true;
		final ResourceBundle RB = ResourceBundle.getBundle("fr.metier.properties.config");
		String mdp1 = RB.getString("mot.de.passe");
		if (!mdp1.equals(mdp)) {
			verif=false;
		}
		return verif;
		}
	
	
}
