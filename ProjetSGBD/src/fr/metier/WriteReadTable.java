package fr.metier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriteReadTable {
	
	
	/**
	 * �crit un nouveau fichier
	 * @param isolateTableName
	 * @param values
	 */
	public void writeFile(String isolateTableName, String values) {
		FileWriter myWriter;
		try {
			myWriter = new FileWriter("C://ENV//BDD//" + isolateTableName + ".cda", true);
			myWriter.write(values + "\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("le fichier existe d�j�");
			e.printStackTrace();
		}
		}
	
	/**
	 * �crit dans un fichier existant
	 * @param searchTableName
	 * @param values
	 */
	public void writeExistingFile(String searchTableName, String values) {
		FileWriter myWriter;
		try {
			myWriter = new FileWriter("C://ENV//BDD//" + searchTableName + ".cda", true);
			myWriter.write(values + "\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("le fichier existe d�j�");
			e.printStackTrace();
		}
		}
	
}
