package fr.control;

import java.io.File;
import java.util.ResourceBundle;

public interface ControlNomFichier {
	
	
	/**
	 * Contr�le du login et mot de passe, retourne un bool�en.
	 * @param login
	 * @return
	 */
	public static boolean controlLogin(String login, String mdp) {
		boolean verif=true;
		final ResourceBundle RB = ResourceBundle.getBundle("fr.metier.properties.config");
		String dbMdp = RB.getString("mot.de.passe");
		String dbLogin= RB.getString("login");
		if (!dbMdp.equals(mdp) && !dbLogin.equals(login)) {
			verif=false;
			System.out.println("Login ou mot-de-passe incorrect");
		}
		return verif;
	}
	
	/**
	 * Retourne un bool�en qui informe si la requ�te est valide
	 * @param request
	 * @return
	 */	
	public static boolean controlRequest(String request) {
		boolean verif=false;
		String [] patterns = new String [9];
		patterns[0] = "SQL \\:\\> CREATE TABLE [a-z]{1,25}\\(([a-z]{1,25}\\, )*[a-z]{1,25}\\)";
		patterns[1] = "SQL \\:\\> INSERT INTO [a-z]{1,25} VALUES\\(([a-z]{1,25}\\, )*[a-z]{1,25}\\)";
		patterns[2] = "SQL \\:\\> SELECT [a-z]{1,25} FROM [a-z]{1,25}";
		patterns[3] = "SQL \\:\\> SELECT [a-z]{1,25} FROM [a-z]{1,25}";
		patterns[4] = "SQL \\:\\> SELECT \\* FROM [a-z]{1,25} Order by [a-z]{1,25} ASC";
		patterns[5] = "SQL \\:\\> SELECT \\* FROM [a-z]{1,25} Order by [a-z]{1,25} DESC";
		patterns[6] = "SQL \\:\\> UPDATE [a-z]{1,25} SET [a-z]{1,25} \\= [a-z]{1,25}";
		patterns[7] = "SQL \\:\\> UPDATE [a-z]{1,25} SET ([a-z]{1,25} \\= [a-z]{1,25}\\, )*[a-z]{1,25} \\= [a-z]{1,25}";
		patterns[8] = "SQL \\:\\> UPDATE [a-z]{1,25} SET [a-z]{1,25} \\= [a-z]{1,25} WHERE [a-z]{1,25} \\= [a-z]{1,25}";
			for (int i=0 ; i<patterns.length ; i++) {
				if (request.matches(patterns[i])) {
					verif=true;
					break;
				}
			}
			return verif;
	}
	
	
	/**
	 * Tant que la m�thode de contr�le controlRequest est false, afficher HELP
	 */
	public static void displayHelp() {
		System.out.println("Seuls les caract�res alphab�tiques minuscules sont accept�s pour le nom de la table, les champs et les valeurs. Veuillez saisir une requ�te valide parmi la liste suivante : "+"\n"+
				"Cr�er une table :      CREATE TABLE nomtable(colonnea, colonneb, colonnec)"+"\n"+
				"Ins�rer des donn�es :  INSERT INTO nomtable VALUES(valeura, valeurb, valeurc)"+"\n"+
				"Affichage des donn�es : SELECT * FROM nomtable"+"\n"+
				"Isoler et afficher par champ : SELECT nomduchamp FROM nomtable"+"\n"+
				"Afficher une table en triant par ordre croissant : SELECT * FROM nomtable Order by nomduchamp ASC"+"\n"+
				"Afficher une table en triant par ordre d�croissant : SELECT * FROM nomtable Order by nomduchamp DESC"+"\n"+
				"Modifier le nom d'un champ : UPDATE nomtable SET nomduchamp = nouvellevaleur"+"\n"+
				"Attribuer de nouvelles valeurs aux champs : UPDATE nomtable SET nomduchamp = nouvellevaleur, nomduchamp = nouvellevaleur"+"\n"+
				"Attribuer une nouvelle valeur � la colonne nomduchamp pour les lignes qui respectent la condition stipul�e avec WHERE : UPDATE nomtable SET nomduchamp = nouvellevaleur WHERE nomduchamp = anciennevaleur");
	}
	
	   /**
	    * M�thode pour �viter la duplication de fichier ou l'�crasement 
	    * @param tab
	    * @return
	    */
	   public static boolean controlExistenceTable(String isolateTableName) {
		   boolean verif=true;
		   File fichier;
		   ResourceBundle rb = ResourceBundle.getBundle("fr.metier.properties.config");
		   fichier = new File(rb.getString("chemin")+isolateTableName+".cda");
		   if (!fichier.exists()) {
			   verif=false;
		   } else {
			   System.out.println("Impossible de cr�er un fichier d�j� existant");
		   }
		   return verif;
	   }
	   
	   /**
	    * M�thode qui renvoie si le fichier saisi en entr�e existe bien pour pouvoir le remplir de donn�es
	    * @param searchTableName
	    * @return
	    */
	   public static boolean controlExistenceInsertDisplay(String searchTableName) {
		   boolean verif=true;
		   File fichier;
		   ResourceBundle rb = ResourceBundle.getBundle("fr.metier.properties.config");
		   fichier = new File(rb.getString("chemin")+searchTableName+".cda");
		   if (fichier.exists()) {
			   verif=false;
		   } else {
			   System.out.println("Le fichier � remplir n'existe pas");
		   }
		   return verif;
	   }

}